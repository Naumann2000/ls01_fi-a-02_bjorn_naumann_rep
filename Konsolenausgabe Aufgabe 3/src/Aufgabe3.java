
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.printf("%-12s%s%10s\n", "Fahrenheiten", "|", "Celsius");
		System.out.printf("------------------------\n");
		System.out.printf("%-12d%s%10.2f\n", -20, "|", -28.89);
		System.out.printf("%-12d%s%10.2f\n", -10, "|", -23.33);
		System.out.printf("%-12d%s%10.2f\n", +0, "|", -17.78);
		System.out.printf("%-12d%s%10.2f\n", +20, "|", -6.67);
		System.out.printf("%-12d%s%10.2f\n", +30, "|", -1.11);
	}

}
