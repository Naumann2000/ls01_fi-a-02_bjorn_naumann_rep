import java.io.InputStream;
import java.util.Scanner;
import java.io.Console;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Benutzerverwaltung {

    public static void main(String[] args) {
        BenutzerverwaltungV10.start();
    }
}

class BenutzerverwaltungV10{
    public static void start(){
        BenutzerListe benutzerListe = new BenutzerListe();
        Scanner tastatur = new Scanner(System.in);

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));

        System.out.println("Was möchsten Sie tun?\n");
        System.out.println("Anmelden (1)");
        System.out.println("Regestrieren (2)\n");
        System.out.println("Ihre Auswahl: ");
        
        int auswahl = tastatur.nextInt();
        
        if (auswahl == 1) {
        	anmelden(tastatur, benutzerListe);
        }else if(auswahl ==2) {
        	registrieren(tastatur, benutzerListe);
        }
        
        
        
    }
    
    public static void anmelden (Scanner tastatur, BenutzerListe benutzerListe) {
    	String name;
    	String passwort;
    	
    	int versuche = 0;
    	
    	while (versuche < 3 ) {
    		System.out.print("Bitte geben Sie Ihren Benutzername ein:");
    		name = tastatur.next();
    		
    		System.out.print("Bitte geben Sie Ihr Passwort ein:");
    		passwort = tastatur.next();
    		
    		Benutzer benutzer = benutzerListe.sucheBenutzer(name);
    		
    		if (benutzer == null || !passwort.equals(benutzer.getPasswort())) {
    			System.out.println("Falscher Benutzername oder Passwort!\n");
    			name = null;
    			passwort = null;
    			versuche ++;
    		} else {
    			System.out.println("Sie sind angemeldet!");
    			benutzer.setAnmeldestatus("online");
    			Date date = new Date();
    			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
    			benutzer.setLetzterLogin(formatter.format(date));
    			return;
    		}
    	}
    	System.out.println("Anlemdung fehlgeschlagen!");
    }
    
    public static void registrieren(Scanner tastatur, BenutzerListe benutzerListe) {
    	String name;
    	String passwort;
    	String passwortBestätigung;
    	boolean nutzerBereitsVorhanden = false;
    	
    	do {
    		System.out.print("Bitte geben Sie Ihren gewünschten Benutzernamen ein:");
    		name = tastatur.next();
    		
    		if (benutzerListe.sucheBenutzer(name) != null) {
    			System.out.println("\nBenutzername bereits vorhanden!\n");
    			nutzerBereitsVorhanden = true;
    		} else {
    			boolean passwörterSindUngleich = true;
    			nutzerBereitsVorhanden = false;
    			
    			do {
	    			System.out.print("Bitte geben Sie Ihr gewnünschtes Passowrt ein:");
	    			passwort = tastatur.next();
	    			
	    			System.out.print("Bitte bestätigen Sie Ihr Passwort:");
	    			passwortBestätigung = tastatur.next();
	    			
	    			if (passwort.equals(passwortBestätigung)) {
	    				benutzerListe.insert(new Benutzer(name, passwort));
	    				System.out.println("\nSie wurden erfolgreich registrtiert, " + name + "!");
	    				passwörterSindUngleich = false;
	    			} else {
	    				System.out.println("\nDie Passwörter stimmen nicht überein!\n");
	    			}
	    			
    			} while(passwörterSindUngleich);
    			
    		}
    	} while (nutzerBereitsVorhanden);
    }
}

class BenutzerListe{
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name){
        // ...
        return true;
    }
    
    public Benutzer sucheBenutzer(String name) {
    	Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b;
            }
            b = b.getNext();
        }
        return null;
    }
}

class Benutzer{
    private String name;
    private String passwort;
    private String anmeldestatus;
    private String letzterLogin;

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
    
    public String getPasswort() {
    	return passwort;
    }
    
    public String getAnmeldestatus() {
    	return anmeldestatus;
    }
    
    public void setAnmeldestatus(String status) {
    	anmeldestatus = status;
    }
    
    public String getLetzterLogin() {
    	return letzterLogin;
    }
    
    public void setLetzterLogin(String datum) {
    	letzterLogin = datum;
    }
    
}