﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double rückgabebetrag;
       char weiter = 'j';

       while(weiter == 'j') {
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

       // Geldeinwurf
       // -----------
       
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag,tastatur);
       
       
       // Fahrscheinausgabe
       // -----------------
       
       fahrkartenAusgeben();
       

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       rückgeldAusgeben(rückgabebetrag);
       
       System.out.println("Möchten Sie noch weitere Fahrkarten kaufen? [j/n]");
       weiter = tastatur.next().charAt(0);
       }
    }
    
    private static double fahrkartenbestellungErfassen(Scanner eingabe) {
	  
    	int auswahlnummer;
    	
	    String[] fahrkartenBezeichnung = new String[10];
	    
	    fahrkartenBezeichnung[0] = "Einzelfahrschein Berlin AB";
	    fahrkartenBezeichnung[1] = "Einzelfahrschein Berlin BC";
	    fahrkartenBezeichnung[2] = "Einzelfahrschein Berlin ABC";
	    fahrkartenBezeichnung[3] = "Kurzstrecke";
	    fahrkartenBezeichnung[4] = "TageskartebBerlin AB";
	    fahrkartenBezeichnung[5] = "TageskartebBerlin CB";
	    fahrkartenBezeichnung[6] = "TageskartebBerlin ABC";
	    fahrkartenBezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
	    fahrkartenBezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
	    fahrkartenBezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
	    
	    
	    double[] fahrkartenPreis = new double[10];
	    
	    fahrkartenPreis[0] = 2.9;
	    fahrkartenPreis[1] = 3.3;
	    fahrkartenPreis[2] = 3.6;
	    fahrkartenPreis[3] = 1.9;
	    fahrkartenPreis[4] = 8.6;
	    fahrkartenPreis[5] = 9.0;
	    fahrkartenPreis[6] = 9.6;
	    fahrkartenPreis[7] = 23.5;
	    fahrkartenPreis[8] = 24.3;
	    fahrkartenPreis[9] = 24.9;
	    
	    
	    System.out.println("Wählen Sie Ihre Wunschfahrkarte aus: ");
	    
	    for (int i = 0; i < fahrkartenBezeichnung.length; i++) {
	    	int auswahl = i + 1;
	    	System.out.printf("%s%.2f%s\n",fahrkartenBezeichnung[i] + " [", fahrkartenPreis[i], " EUR] (" + auswahl + ")");
	    }
	    
	    System.out.print("\nIhre Wahl: ");
	    
	    auswahlnummer = eingabe.nextInt();
	    
	    while (auswahlnummer < 0 || auswahlnummer > 10) {
	    	
	    	System.out.println("Fehler! Ungültige Ausahl");
	    	System.out.print("\nIhre Wahl: ");
	    	auswahlnummer = eingabe.nextInt();
	       }
	    
	    double fahrkartenAuswahlPreis = fahrkartenPreis[auswahlnummer-1];
	    
	    System.out.print("Anzahl der Fahrkarten: ");
	    double anzahlDerFahrkarten = eingabe.nextInt();
	    
	    while(anzahlDerFahrkarten > 10)
	    {
	    	System.out.println("Sie können maximal 10 Tickets kaufen! Azahl der Tickets wurde auf 1 reduziert.\n");
	    	
	    	System.out.print("Anzahl der Fahrkarten: ");
		    anzahlDerFahrkarten = 1;
	    }
	    
	    while(anzahlDerFahrkarten < 1) {
	    	
	    	System.out.println("Geben sie bitte nur eine positive Anzahl an Tickets ein! Azahl der Tickets wurde auf 1 gesetzt.\n");
	    	
	    	System.out.print("Anzahl der Fahrkarten: ");
		    anzahlDerFahrkarten = 1;
	    	
	    }
	    	
	    	
	    return (double) anzahlDerFahrkarten * fahrkartenAuswahlPreis;
    }
    
    private static double fahrkartenBezahlen(double betrag, Scanner tastatur) {
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < betrag)
        {
     	   System.out.printf("%s%.2f%s\n","Noch zu zahlen: " , (betrag - eingezahlterGesamtbetrag) , "Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
          
        }
        return eingezahlterGesamtbetrag - betrag;
    }
    
    public static void fahrkartenAusgeben() {

        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}}
        System.out.println("\n\n");
    }
    
    public static void rückgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("%s%.2f%s\n","Der Rückgabebetrag in Höhe von " , rückgabebetrag , " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.00;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.00;
            }
            while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
    
}